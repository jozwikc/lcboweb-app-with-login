package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLengthLoginRegular( ) {
		
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
		
	}
	
	@Test(expected = NullPointerException.class)
	public void testIsValidLengthLoginException( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( null ) );
	}
	
	@Test
	public void testIsValidLengthLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLengthLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ramse" ) );
	}
	
	
	@Test
	public void testIsValidStartLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses21" ) );
	}
	
	@Test
	public void testIsValidStartLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$%^" ) );
	}
	
	@Test
	public void testIsValidStartLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r2amses" ) );
	}
	
	@Test
	public void testIsValidStartLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "2ramses" ) );
	}
}
