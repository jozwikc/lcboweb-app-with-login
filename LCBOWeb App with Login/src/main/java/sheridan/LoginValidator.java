package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		String regex = "^[A-Za-z]\\w{5,}$";
		
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(loginName);
		
		return m.matches();
	}
}
